//
//  EasyDiUi.swift
//  SA-100
//
//  Created by George Kiriy on 17/04/2018.
//  Copyright © 2018 S Media Link. All rights reserved.
//

import EasyDi
import Foundation
import UIKit

public protocol AnyResolvingAssembly {
    func inject(into: AnyObject)
}

public protocol ResolvingAssembly: AnyResolvingAssembly {
    associatedtype InstanceType: NSObjectProtocol
    func inject(into instance: InstanceType)
}

extension ResolvingAssembly {
    public func inject(into any: AnyObject) {
        guard let concrete = any as? InstanceType else {
            fatalError()
        }
        inject(into: concrete)
    }
}

public class Application<Delegate>: UIApplication where Delegate: InjectableByTag {
    public override var delegate: UIApplicationDelegate? {
        willSet {
            guard let delegate = newValue as? Delegate else {
                return
            }
            delegate.assembly.inject(into: delegate)
        }
    }
}

public protocol Injectable: class {
    var assembly: AnyResolvingAssembly { get }
}

public protocol InjectableByTag: Injectable {
    associatedtype InstantiationAssembly: Assembly & ResolvingAssembly
}

extension InjectableByTag {
    public var assembly: AnyResolvingAssembly {
        return DIContext.defaultInstance.assembly() as InstantiationAssembly
    }
}

extension NSObject {
    @objc public var diTag: Any? {
        get {
            return nil
        }
        set {
            if let injectable = self as? Injectable {
                injectable.assembly.inject(into: self)
            }
        }
    }
}
