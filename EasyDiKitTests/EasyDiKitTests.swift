import XCTest
import EasyDi
import EasyDiKit


public class AppDelegate: NSObject {
    var value: Int!
}

public class AppDelegateAssembly: Assembly, ResolvingAssembly {
    public func inject(into appDelegate: AppDelegate) {
        defineInjection(into: appDelegate) {
            $0.value = 1
            return $0
        }
    }
}

extension AppDelegate: InjectableByTag {
    public typealias InstantiationAssembly = AppDelegateAssembly
}


class Tests: XCTestCase {

    func linkFix() { print() }

    func testExample() {
        let appDelegate = AppDelegate()
        appDelegate.assembly.inject(into: appDelegate)
        XCTAssertEqual(appDelegate.value, 1)
    }
}
